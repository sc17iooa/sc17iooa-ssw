from __future__ import print_function

import json
import urllib
import boto3
import datetime

print('Loading funtion...')

def process_gift_code(message, context):

    print("Receive message from Step Function")
    print(message)

    response = {}
    response['TransactionType'] = message['TransactionType']
    response['Timestamp'] = datetime.datetime.now().strftime("%d-%m-%Y at %H:%M:%S")
    response['Message'] = 'Verified'

    return response

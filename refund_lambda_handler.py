from __future__ import print_function

import json
import urllib
import boto3
import datetime

print('Loading funtion...')

def process_refund(message, context):

    #Input Example
    #{ 'TransactionType': 'REFUND'}

    #1. Log input message
    print("Receive message from Step Function")
    print(message)
    #2. Construct response object
    response = {}
    response['TransactionType'] = message['TransactionType']
    response['Timestamp'] = datetime.datetime.now().strftime("%d-%m-%Y at %H:%M:%S")
    response['Message'] = 'Processed Refund'

    return response 
